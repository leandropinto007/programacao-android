package layouts.estgf.ipp.pt.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MyListAdapter extends ArrayAdapter<Pais> {

    private ArrayList<Pais> mPais;
    private Context mContext;

    public MyListAdapter(Context mContext, ArrayList<Pais> mPais){
        super(mContext,R.layout.activity_linha,mPais);
        this.mPais = mPais;
        this.mContext = mContext;


    }
    @NonNull
    @Override

    public View getView(int position, View convertView, ViewGroup parent){

        View v = convertView;
        if(v == null){
            LayoutInflater vi = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.activity_linha,null);

        }

        TextView name = v.findViewById(R.id.Nome);
        name.setText(mPais.get(position).getName());

        TextView continente = v.findViewById(R.id.Continente);
        continente.setText(mPais.get(position).getContinente());
        return v;

    }
}
