package layouts.estgf.ipp.pt.dialogs;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends ListActivity {

    private ArrayList<Pais> mList = new ArrayList<Pais>();
    private MyListAdapter mAdapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setInfo(); // vai pegar nos paises ja adicionados abaixo


        mAdapter = new MyListAdapter(MainActivity.this, mList);
        setListAdapter(mAdapter);
    }

    @Override

    protected void onListItemClick(ListView l, View v, final int position, long id){
        super.onListItemClick(l,v,position,id);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_dialog);
        String pais = mList.get(position).getName();
        Resources res = getResources();
        String removido = pais +" "+ res.getString(R.string.toast_rem);


        String remove = getString(R.string.message_remove)+" "+
               pais +" "+getString(R.string.list_rem);

        builder.setMessage(remove);
        builder.setPositiveButton(R.string.btn_positivo, (dialog, dialogId)->{
            mList.remove(position);
            mAdapter.notifyDataSetChanged();
            Toast.makeText(this, removido,Toast.LENGTH_SHORT).show();
        });


        builder.setNegativeButton(R.string.btn_negativo, (dialog, dialogId) -> dialog.dismiss());
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setInfo() {
        mList.add(new Pais("Portugal", "Europe"));
        mList.add(new Pais("Spain", "Europe"));
        mList.add(new Pais("Germany", "Europe"));
        mList.add(new Pais("Netherlands", "Europe"));
        mList.add(new Pais("Norway", "Europe"));
        mList.add(new Pais("Italy", "Europe"));
        mList.add(new Pais("Greece", "Europe"));
        mList.add(new Pais("Poland", "Europe"));
        mList.add(new Pais("USA", "America"));
        mList.add(new Pais("Brazil", "America"));
        mList.add(new Pais("Venezuela", "America"));
        mList.add(new Pais("Chile", "America"));
        mList.add(new Pais("Mexico", "America"));
        mList.add(new Pais("Cuba", "America"));
        mList.add(new Pais("Argentina", "America"));
        mList.add(new Pais("Niger", "Africa"));
        mList.add(new Pais("Angola", "Africa"));
        mList.add(new Pais("Egypt", "Africa"));
        mList.add(new Pais("South Africa", "Africa"));
        mList.add(new Pais("Mozambique", "Africa"));
        mList.add(new Pais("Japan", "Asia"));
        mList.add(new Pais("Indonesia", "Asia"));
        mList.add(new Pais("Russia", "Asia"));
        mList.add(new Pais("South Korea", "Asia"));
    }
}






