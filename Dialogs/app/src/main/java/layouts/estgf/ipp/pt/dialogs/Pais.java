package layouts.estgf.ipp.pt.dialogs;

public class Pais {
    private String name;
    private String continente;

    public Pais(String name, String continente) {
        this.name = name;
        this.continente = continente;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinente() {
        return continente;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }
}
