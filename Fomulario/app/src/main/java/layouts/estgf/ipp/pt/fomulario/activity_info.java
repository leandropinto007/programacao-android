package layouts.estgf.ipp.pt.fomulario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class activity_info extends AppCompatActivity {

    static final String AGE = "Age";
    static final String NAME = "Name";
    static final String DESC = "Desc";
    static final String GENDER = "Gender";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        String age = getIntent().getStringExtra(AGE);
        String name = getIntent().getStringExtra(NAME);
        String desc = getIntent().getStringExtra(DESC);
        String gender = getIntent().getStringExtra(GENDER);

        TextView nameView = findViewById(R.id.name);
        nameView.setText(name);
        TextView ageView = findViewById(R.id.age);
        ageView.setText(age);
        TextView genderView = findViewById(R.id.gender);
        genderView.setText(gender);
        TextView descView = findViewById(R.id.desc);
        descView.setText(desc);
    }
}

