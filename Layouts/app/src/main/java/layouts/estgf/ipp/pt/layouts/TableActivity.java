package layouts.estgf.ipp.pt.layouts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;

public class TableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        Button teste = (Button) findViewById(R.id.button_id);

        //radioButtonSelect
        final RadioButton radio1 = (RadioButton)findViewById(R.id.radio1);
        radio1.setChecked(true);

        //spinner
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.paises, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        teste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testeIntent = new Intent(TableActivity.this.getApplicationContext(), MainActivity.class);
                testeIntent.putExtra("teste1", "teste2");
                startActivity(testeIntent);
            }
        });
    }
}
